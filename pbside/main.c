#include<stdio.h>
#include<stdlib.h>
#include<sqlite3.h>
#include<time.h>
#include<signal.h>
#include<string.h>
#include<sys/prctl.h>
#include<linux/seccomp.h>
#include<unistd.h>
//#include"syscall-reporter.h"
//#include"seccomp-bpf.h"
#include"config.h"
#include"tools.h"

const int NON_ind = 1;
const int AC_ind = 2;
const int WA_ind = 3;
const int TLE_ind = 4;
const int CE_ind = 5;
const int SE_ind = 6;

int stoi(const char* str){
    int res=0;
    int lx = 0;for(;str[lx] != '\0';lx++){
        res = res*10 + str[lx] - '0';
    }
    return res;
}

typedef struct _JudgeResult{ int result; int exetime;}JudgeResult;

JudgeResult judge(char* codefile, int prb_id){
    char srcfile[100], tarfile[100];
    execmd("rm testarea/*");
    sprintf(srcfile, "%s%s", path_code, codefile);
    sprintf(tarfile, "%s%s", path_testarea, codefile);
    printf("code src file: %s\n", srcfile);
    printf("code tar file: %s\n", tarfile);
    copy(srcfile, tarfile);
    sleepms(10);
	copy("./sandbox/EasySandbox.so", "./testarea/");
    sleepms(10);
    chdir(path_testarea);
	testls();
	
	/*char* tmp_str = (char*)get_current_dir_name();
	puts(tmp_str);
	free(tmp_str);*/
    
	JudgeResult result = {1, 0};
    sleepms(10);

	if(checkfile(codefile) == false){
		fprintf(stderr, "cp fail.\n");
		testls();
		result.result = SE_ind;
		goto judgeok;
	}

	gcc(codefile);
	sleepms(10);

    if(checkfile("t123.out") == false){
        result.result = CE_ind;
        goto judgeok;
    }
    //t123.out
    char testfilein[100], testfileout[100];
    sprintf(testfilein, "../%s%d/1.in", path_testdata, prb_id);
    fprintf(stderr, "testfilein : %s\n", testfilein);
    sprintf(testfileout, "../%s%d/1.out", path_testdata, prb_id);

	clock_t tstart, tend; int timediff;
    tstart = clock(); 
    pid_t pid = fork();
    if(pid == 0){
        freopen(testfilein, "r", stdin);
        freopen("test.out", "w", stdout);
        //setrlimit
        //setuid(65534);
		chroot(".");
        char* argv[] = {"./t123.out", (char*)0};
		char* envs[] = {"LD_PRELOAD=./EasySandbox.so", (char*)0};
		fprintf(stderr, "ready to start\n");
        execve("./t123.out", argv, envs);
        fprintf(stderr, "execve err !\n");
        exit(0);
    }
    int tmp;
    int time_limit = 3010;
    while(1){
        tend = clock();
        timediff = (int)((((float)tend - (float)tstart)/1000000.0F)*1000);
        if(waitpid(pid, &tmp, WNOHANG) == 0){
            if(timediff >= time_limit){
                kill(pid, SIGKILL);
				wait(NULL);
                result.result = TLE_ind;
                goto judgeok;
            }
        }else
            break;
    }
    result.exetime = timediff; 
    /*more("test.out");
	printf("=======\n");
    more(testfileout);
	printf("=======\n");*/
	remove_first_line("test.out");
	if(diff("test.out", testfileout))
        result.result = WA_ind;
    else
        result.result = AC_ind;

judgeok:
    chdir("../");
    return result;
}
static int callback(void* data, int argc, char** argv, char** azColName){
    printf("callback\n");
    return 0;
}
int main(){
    chdir("../");
	sqlite3* db;
	char* errmsg = NULL;
    if(sqlite3_open("main.db", &db)){
	    printf("open db error !\n");
        return 0;
    }
    while(1){
        //printf("---------\n");
    	char** res;
    	int nrow = 1, ncol = 1;
	    if(sqlite3_get_table(db, "SELECT id,code,probid FROM submits WHERE verdict=1;"
                                    , &res, &nrow, &ncol, &errmsg) == SQLITE_OK){
		    //printf("nrow  = %d, ncol = %d\n", nrow, ncol);
            char filename[1000];
			int probid, submitid;
            if(nrow){
                strcpy(filename, res[4]);
                probid = stoi(res[5]);
                submitid = stoi(res[3]);
                JudgeResult jr = judge(filename, probid);
                printf("Result: %d\n", jr.result);
                char sqlres[1000];
                sprintf(sqlres, "UPDATE submits set verdict = %d,exetime = %d where id = %d;", 
                                    jr.result, jr.exetime, submitid);
                char tmp[] = "check point1\n";
                int rc = sqlite3_exec(db, sqlres, callback,(void *) tmp, &errmsg);
                if(rc != SQLITE_OK)
                    printf("Update err occur!\n");
                else{
                    printf("Update OK %d\n", submitid);
				}
            }else{
                //printf("No submittion to deal with.\n");
                sleepms(10);
            }
    	}else{
	    	printf("ERR on selectint submits\n");
    	}
    }
	sqlite3_close(db);
	return 0;
}
