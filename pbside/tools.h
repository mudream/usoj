#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/stat.h>
#include<sys/time.h>
#include<sys/resource.h>
#include"bool.h"
/*void startexe(char* src){
    int chidlExitStatus;
    pid_t pid;
    int status;
    if(!src) return;
    pid = fork();
    execmd("./t123.out < ../testdata/hello/1.in > 1.out");
    if(pid == 0){
        execl("./t123.out", "./t123.out")*/
void sleepms(int);
void testls();
bool checkfile(char*);
void copy(char* src, char* tar){
    int childExitStatus;
    pid_t pid;
    int status;
    if(!src || !tar) return;
    pid = fork();
    if(pid == 0){
        execl("/bin/cp", "/bin/cp", src, tar, (char*)0);
    }else if(pid < 0){
        printf("fail to start process\n");
    }else{
        pid_t ws = waitpid(pid, &childExitStatus, WNOHANG);
        if(ws == -1){
			return;
		}
        if(WIFEXITED(childExitStatus)){
            status = WEXITSTATUS(childExitStatus);
            return;
        }else if(WIFSIGNALED(childExitStatus)){
            return;
        }else if(WIFSTOPPED(childExitStatus)){
            return;
        }
    }
    return;
}
void remove_first_line(const char* fn){
	char cmd[100];
	sprintf(cmd, "echo \"$(tail -n +2 %s)\" > %s", fn, fn);
	fprintf(stderr, "command: %s\n", cmd);
	FILE *fpc = popen(cmd, "r");
	char c;
	while(fscanf(fpc, "%c", &c) != EOF);
	pclose(fpc);
	return;
}
void gcc(char* fn){
    char cmdmod[] = "gcc %s -o %s";
    char cmd[100];
    sprintf(cmd, cmdmod, fn, "t123.out");
	//sleepms(10);
	//fflush(stdout);
	printf("ready to gcc: %s\n", cmd);
	testls();
	pid_t pid = fork();
	if(pid == 0){
        struct rlimit rlt;
		rlt.rlim_cur = 60;
		rlt.rlim_max = 60;
		setrlimit(RLIMIT_CPU, &rlt);
		//fprintf(stderr, "test:\n");
		//testls();
		/*char* tmp_str = (char*)get_current_dir_name();
		puts(tmp_str);
		free(tmp_str);*/

		while(checkfile(fn) == false){
			// Wait for file already.
			fprintf(stderr, "waiting...\n");
		}

	    FILE *fpc;
		char ch[10000]; // TODO: clean?

		/*fprintf(stderr, "abc[");
		fpc = popen("ls -al", "r");
		while(fscanf(fpc, "%s", ch) != EOF){
			fprintf(stderr, "[%s]", ch);
		}
		fprintf(stderr, "]abc");*/

		//system(cmd);
		fpc = popen(cmd, "r");
		/*fprintf(stderr, "===================123\n");
		while(fscanf(fpc, "%s", ch) != EOF){
			fprintf(stderr, "[%s]", ch);
		}
		sleepms(10);
		fprintf(stderr, "===================\n");*/
		pclose(fpc);
		exit(0);
	}
	wait(NULL);
    return;
}
void execmd(char* cmd){
    FILE *fpc = popen(cmd, "r");
    pclose(fpc);
}
bool checkfile(char* fn){
	struct stat buf;
	return stat(fn, &buf) == 0;
    //return access(fn, F_OK) == 0;
}

bool diff(char* fn1, char* fn2){
    struct stat sts;
    if(stat(fn1, &sts) == -1) return true;
    if(stat(fn2, &sts) == -1) return true;
    char cmd[10000];
    sprintf(cmd, "diff %s %s", fn1, fn2);
    FILE *fpc = popen(cmd, "r");
/*    char c;
    fread(&c, 1, 1, fpc);*/
    char str[1000];
    int cnt = 0;
    while(fscanf(fpc, "%s", str) != EOF){
        printf("[%s]\n", str);
        cnt++;
		break;
    }
    return cnt > 0;
}
void more(char* file){
    FILE* fp = fopen(file, "r");
    char str[10000];
    while(fscanf(fp, "%s", str) != EOF){
        printf("[%s]\n", str);
    }
    return;
}

void sleepms(int k){
    int lx;
    for(lx = 0;lx/10000 < k*1000;lx++);
    return;
}

void testls(){
    int childExitStatus;
    pid_t pid;
    int status;
    pid = fork();
    if(pid == 0){
		char *args[] = {"/bin/ls", NULL};
		execv("/bin/ls", args);
		perror("exec err");
		exit(0);
    }else if(pid < 0){
        printf("fail to start process\n");
    }else{
        pid_t ws = waitpid(pid, &childExitStatus, WNOHANG);
        if(ws == -1){
			return;
		}
        if(WIFEXITED(childExitStatus)){
            status = WEXITSTATUS(childExitStatus);
            return;
        }else if(WIFSIGNALED(childExitStatus)){
            return;
        }else if(WIFSTOPPED(childExitStatus)){
            return;
        }
    }
    return;
}
