import os
import tornado.ioloop
import tornado.web
import tornado.template
import db

tpldr = tornado.template.Loader("templ")

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        probs = db.ProbHandler.inst.list_prob()
        self.write(tpldr.load("index.html").generate(
            probs = probs
        ))
        self.finish()


class ProbHandler(tornado.web.RequestHandler):
    def get(self, pid):
        prob = db.ProbHandler.inst.get_prob(pid)
        self.write(tpldr.load("prob.html").generate(
            prob = prob
        ))
        self.finish()


class StatusHandler(tornado.web.RequestHandler):
    def get(self):
        submits = db.SubmitHandler.inst.list_submit()
        self.write(tpldr.load("status.html").generate(
            submits = submits
        ))
        self.finish()


class CodeHandler(tornado.web.RequestHandler):
    def get(self, sid):
        code = db.SubmitHandler.inst.get_code(sid)
        self.write(tpldr.load("code.html").generate(
            code = code
        ))
        self.finish()


class SubmitHandler(tornado.web.RequestHandler):
    def get(self, pid):
        prob = db.ProbHandler.inst.get_prob(pid)
        self.write(tpldr.load("submit.html").generate(
            pid = pid
        ))
        self.finish()

    def post(self, pid):
        pid = int(self.get_argument("pid"))
        name = self.get_argument("name")
        code = self.get_argument("code")
        print("get data:", pid, code, name)
        db.SubmitHandler.inst.append_submit(name, pid, code)
        self.redirect("/status")


if __name__ == "__main__":
    settings = {
        "static_path": os.path.join(os.path.dirname(__file__), "static"),
        "cookie_secret": "298KJDFKHJSlkGkHJjHiJH&g",
    }
    app = tornado.web.Application([
        (r"/", MainHandler),
        (r"/prob/(.*)", ProbHandler),
        (r"/submit/(.*)", SubmitHandler),
        (r"/code/(.*)", CodeHandler),
        (r"/status", StatusHandler),
    ], **settings)
    app.listen(1240)
    db.initdb()
    tornado.ioloop.IOLoop.instance().start()
