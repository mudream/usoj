import sqlite3 as lite

def initdb():
    con = lite.connect("../main.db")
    pb = ProbHandler(con)
    sb = SubmitHandler(con)


class ProbHandler:
    inst = None
    def __init__(self, con):
        ProbHandler.inst = self
        self.con = con

    def list_prob(self):
        cur = self.con.cursor()
        cur.execute("SELECT id,title,content FROM probs")
        probs = []
        for row in cur.fetchall():
            prob = {}
            prob["id"] = row[0]
            prob["title"] = row[1]
            prob["content"] = row[2]
            probs.append(prob)

        return probs

    def append_prob(self):
        pass

    def get_prob(self, pid):
        pid = int(pid)
        cur = self.con.cursor()
        cur.execute("SELECT id,title,content FROM probs WHERE id=?", (pid, ))
        row = cur.fetchall()[0]
        return {"id" : row[0],
                "title": row[1],
                "content": row[2]}


class SubmitHandler:
    inst = None
    def __init__(self, con):
        SubmitHandler.inst = self
        self.con = con

    def name_gen(self):
        import time
        import random
        mmstr = "abcdefghijklmnopqrstuvwxyz"
        rdstr = ""
        for _ in range(0, 5):
            rdstr += (str)(random.choice(mmstr))
        return (str)(int(time.time())) + rdstr + ".c"

    def list_submit(self):
        cur = self.con.cursor()
        cur.execute("SELECT id,who,code,verdict,probid,exetime FROM submits ORDER BY id DESC"
                    )
        submits = []
        for row in cur.fetchall():
            submit = {}
            submit["id"] = row[0]
            submit["who"] = row[1]
            submit["code"] = row[2]
            submit["probid"] = row[4]
            submit["verdict"] = row[3]
            submit["exetime"] = row[5]
            submits.append(submit)

        return submits

    def get_code(self, sid):
        sid = int(sid)
        cur = self.con.cursor()
        cur.execute("SELECT id, code FROM submits WHERE id=?", (sid, ))

        code = {}
        datas = cur.fetchall()
        if(len(datas) == 0):
            return None
        code["id"] = datas[0][0]
        fn = datas[0][1]

        f = open("../codes/" + fn, "r")
        readstr = ""
        while True:
            line = f.readline()
            if not line : break
            readstr += line
        f.close()

        code["code"] = readstr
        return code

    def append_submit(self, name, pid, code):
        pid = int(pid)
        if(len(code) <= 10): return False
        if(pid <= 0): return False
        if(len(name) >= 10): return False
        cur = self.con.cursor()
        cur.execute("SELECT COUNT(id) FROM submits")
        rowcount= cur.fetchall()[0][0]
        print("rowcount=", rowcount)
        codename = self.name_gen()
        codefile = open("../codes/" + codename, "w")
        codefile.write(code)
        codefile.close()
        cur.execute("INSERT INTO submits(id, who,code,verdict,probid) VALUES(?, ?, ?, 1, ?)", 
                    (rowcount ,name, codename, pid))
        self.con.commit()
        return True
